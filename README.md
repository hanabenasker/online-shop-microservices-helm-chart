# Online Shop Microservices Helm Chart

This repository contains a Helmfile that deploys various microservices for [GCP's microservices demo](https://github.com/GoogleCloudPlatform/microservices-demo) using Kubernetes.

The file **config.yaml** contains the configuration of all the microservices without using Helm charts.
